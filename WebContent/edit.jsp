<%@page import="com.vk.service.StudentServiceImpl"%>
<%@page import="com.vk.service.StudentService"%>
<%@page import="com.vk.service.StudentServiceImpl,com.vk.model.StudentBean "%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Update Student Details</title>
</head>
<body>

<%  
int studentid = Integer.parseInt(request.getParameter("studentid"));  
StudentService studentService = new StudentServiceImpl();
StudentBean student = studentService.getStudentByStudentId((studentid));
%>  

	<h1>Update Student Details</h1>
	
	<form action="editservlet" method="post">  
	<table> 
	<tr>
	<td><input type="hidden" name="studentid" value="<%= student.getStudentId()%>"/>
		</td>
	</tr>
	
	<tr>
		<td>Enrollment:</td>
			<td><input type="text" name="enrollment" value=  "<%= student.getEnrollment()%>" />
		</td>
	</tr>  
	<tr>
		<td>Name:</td>
			<td><input type="text" name="name" value= "<%= student.getName()%>" />
		</td>
	</tr>  
	<tr> 
		<td>Sarname:</td>
			<td><input type="text" name="sarname" value= "<%= student.getSarname()%>" />
		</td>
	</tr>  
	<tr>
		<td>Contact No:</td>
			<td><input type="text" name="contactNo" value= "<%= student.getContactNo()%>"/>
		</td>
	</tr>  
	<tr>
		<td>City:</td>
			<td><input type="text" name="city" value= "<%= student.getCity()%>"/>
		</td>
	</tr> 
	<tr>
		<td >
			<input type="submit" value="Save"/>
		</td>
	</tr>  
	</table>
</form> 
	
</body>
</html>