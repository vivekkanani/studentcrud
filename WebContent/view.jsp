

<%@page import="com.vk.service.StudentServiceImpl"%>
<%@page import="com.vk.service.StudentService"%>
<%@page import="java.util.List"%>
<%@page import="com.sun.corba.se.impl.orb.ParserTable.TestAcceptor1"%>
<%@page import="com.vk.service.StudentServiceImpl , com.vk.model.StudentBean"%>
<%@page import="com.vk.dao.StudentDao" %>
<%@page import="com.vk.service.StudentService" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>View Students Details</title>
</head>
<body>

	<a href ='form.jsp'>Add New Student</a>
 	<h1>Students List</h1>
	<%
		StudentService studentService = new StudentServiceImpl();
		List<StudentBean> list = studentService.getAllStudents();

		request.setAttribute("list", list);
	%>
	<table border='1' width='100%'>
     <tr><th>StudentId</th>
    		<th>Enrollment</th>
     		<th>Name</th>
     		<th>Sarname</th>
     		<th>ContactNo</th>
     		<th>City</th>
            <th>Edit</th>
            <th>Delete</th></tr> 
            
           
    
            <c:forEach items="${list}" var="student">  
     
           		<tr>
                <td>${student.getStudentId()}</td>
      			<td>${student.getEnrollment()}</td>
      			<td>${student.getName()}</td>
      			<td>${student.getSarname()}</td>
                <td>${student.getContactNo()}</td>
                <td>${student.getCity()}</td>
                
            
               <td> <a href='edit.jsp?studentid=${student.getStudentId()}'>Edit</a></td> 
               <td><a href= 'deleteservlet?studentid=${student.getStudentId()}' onclick = "return confirm('Are you sure you want to delete?')" >Delete</a></td>
               </tr>		
            
              </c:forEach>
         </table>
       

</body>
</html>