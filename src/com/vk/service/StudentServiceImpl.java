package com.vk.service;

import java.util.List;

import com.vk.dao.StudentDao;
import com.vk.dao.StudentDaoImpl;
import com.vk.model.StudentBean;;


public  class StudentServiceImpl implements StudentService{

	
		
			@Override
			public int updateStudent(StudentBean student) {
				StudentDao studentDao = new StudentDaoImpl();
				int status = studentDao.updateStudent(student);  
				return status;
			}
			
			@Override
			public int saveStudent(StudentBean student) {
			StudentDao studentDao = new StudentDaoImpl();
			int status = studentDao.saveStudent(student);
			return status;
			}
			
			@Override
			public StudentBean getStudentByStudentId(int studentid) {
				StudentDao studentDao = new StudentDaoImpl();
				StudentBean student = studentDao.getStudentByStudentId((studentid));
				return student;
			}
			
			@Override
			public List<StudentBean> getAllStudents() {
				StudentDao studentDao = new StudentDaoImpl();
				List<StudentBean> list = studentDao.getAllStudents();
				return list;
			}
			
			@Override
			public int deleteStudent(int studentid) {
				StudentDao studentDao = new StudentDaoImpl();
				int status = studentDao.deleteStudent(studentid);
				return status;
			}
	
	}

	
