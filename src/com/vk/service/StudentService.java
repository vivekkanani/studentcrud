package com.vk.service;

import java.util.List;

import com.vk.model.StudentBean;

public interface StudentService {
	
	    public int saveStudent(StudentBean student); 
	    public int updateStudent(StudentBean student) ;
	    public int deleteStudent(int studentid);
	    public List<StudentBean> getAllStudents();
	    public StudentBean getStudentByStudentId(int studentid);
	
		
		}


	


