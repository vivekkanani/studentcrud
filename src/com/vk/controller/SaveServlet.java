package com.vk.controller;


import javax.servlet.http.HttpServlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import com.vk.model.StudentBean;
import com.vk.service.StudentService;
import com.vk.service.StudentServiceImpl;


@WebServlet("/saveservlet")
public  class SaveServlet extends HttpServlet  {
	private static final long serialVersionUID = 1L;

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
	    response.setContentType("text/html");  
        PrintWriter out =response.getWriter();  //printwriter :-print anything on the browser's page
        
        String enrollment = request.getParameter("enrollment");
        String name = request.getParameter("name");  
        String sarname = request.getParameter("sarname");  
        String contactNo = request.getParameter("contactNo");  
        String city = request.getParameter("city");
        
        System.out.println("enrollment ------>"+enrollment);
        System.out.println("name -------->"+name);
        System.out.println("sarname ------>"+sarname);
        System.out.println("contact_no ----->"+contactNo);
        System.out.println("city ------->"+city);
        
        StudentBean  student =new StudentBean(); 
        student.setEnrollment(enrollment);
        student.setName(name);  
        student.setSarname(sarname);
        student.setContactNo(contactNo);
        student.setCity(city);
    
	
		if (enrollment.isEmpty() || name.isEmpty() || sarname.isEmpty() || contactNo.isEmpty() || city.isEmpty()) {
			
			RequestDispatcher req = request.getRequestDispatcher("form.jsp");
			req.include(request, response);
			out.println("Sorry! unable to save record");
		} else {
			
			RequestDispatcher req = request.getRequestDispatcher("form.jsp");
			req.include(request, response);
			out.print("<p>Record saved successfully!</p>");
			out.close();  
			
			
			StudentService studentService = new StudentServiceImpl();
			studentService.saveStudent(student);
	
	}

} 
}
 
