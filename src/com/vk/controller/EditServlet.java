package com.vk.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



import com.vk.model.StudentBean;
import com.vk.service.StudentService;
import com.vk.service.StudentServiceImpl;


@WebServlet("/editservlet")
public class EditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
  
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		int studentid = Integer.parseInt(request.getParameter("studentid"));
	
		String enrollment = request.getParameter("enrollment");
		String name = request.getParameter("name");
		String sarname = request.getParameter("sarname");
		String contactNo = request.getParameter("contactNo");
		String city = request.getParameter("city");
		
		System.out.println("enrollment ------>"+enrollment);
        System.out.println("name -------->"+name);
        System.out.println("sarname ------>"+sarname);
        System.out.println("contactNo ----->"+contactNo);
        System.out.println("city ------->"+city);
        System.out.println("studentid------>"+studentid);
        
        StudentBean  student = new StudentBean(); 
        student.setEnrollment(enrollment);
        student.setName(name);  
        student.setSarname(sarname);
        student.setContactNo(contactNo);
        student.setCity(city);
        student.setStudentId(studentid);
      
        StudentService studentService = new StudentServiceImpl();
        int status = studentService.updateStudent(student);  
        if(status>0){  
        	
        	
            response.sendRedirect("view.jsp");  
        }else{  
            out.println("Sorry! unable to update record");  
        }  
          
        out.close();  
	}

}
