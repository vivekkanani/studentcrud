package com.vk.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.vk.service.StudentService;
import com.vk.service.StudentServiceImpl;



@WebServlet("/deleteservlet")
public class DeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
	
		int studentid = Integer.parseInt(request.getParameter("studentid"));  
		StudentService studentService = new StudentServiceImpl();
		studentService.deleteStudent(studentid);
		response.sendRedirect("view.jsp"); 
	}

}
