package com.vk.model;

public class StudentBean {
	private int studentid;  
	private String enrollment;
	private String name;
	private String sarname;
	private String contactNo;
	private String city;
	
	public int getStudentId() {
		return studentid;
	}
	public void setStudentId(int studentid) {
		this.studentid = studentid;
	}
	public String getEnrollment() {
		return enrollment;
	}
	public void setEnrollment(String enrollment) {
		this.enrollment = enrollment;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSarname() {
		return sarname;
	}
	public void setSarname(String sarname) {
		this.sarname = sarname;
	}
	public String getContactNo() {
		return contactNo;
	}
	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	
		
}