package com.vk.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.vk.model.StudentBean;

public class StudentDaoImpl extends CommonDao implements StudentDao{

	@Override
	public int saveStudent(StudentBean student) {
		 int status = 0; 
	        try{  
	            Connection con = CommonDao.getConnection();
	            PreparedStatement ps = con.prepareStatement(  //for use sql query like update insert select
	            		" insert into  studentinfo(Enrollment,Name,Sarname,ContactNo, City )values (?,?,?,?,?)");  
	            ps.setString(1,student.getEnrollment());  //object
	            ps.setString(2,student.getName());  
	            ps.setString(3,student.getSarname());  
	            ps.setString(4,student.getContactNo());  
	            ps.setString(5,student.getCity());
	           
	            status = ps.executeUpdate(); // object (return)
	            System.out.println("status ....."+status);
	            con.close();

	        	}catch(Exception ex){
	        	  ex.printStackTrace(); //throwable
	        
	        	}  
	        
			return status;
	}

	@Override
	public int updateStudent(StudentBean student) {
		 int status = 0;  
	        try{  
	        	Connection con = CommonDao.getConnection();  
	            PreparedStatement ps = con.prepareStatement("update  studentinfo set Enrollment = ? ,Name = ? ,Sarname = ? ,ContactNo = ? ,City = ? where StudentId = ?");
	            ps.setString(1,student.getEnrollment());
	            ps.setString(2,student.getName());
	            ps.setString(3,student.getSarname());
	            ps.setString(4,student.getContactNo() );
	            ps.setString(5,student.getCity());
	            ps.setInt(6,student.getStudentId());
	            
	        
	            System.out.println(student.getEnrollment());
	            System.out.println(student.getName());
	            System.out.println(student.getSarname());
	            System.out.println(student.getContactNo());
	            System.out.println(student.getCity());
	            System.out.println(student.getStudentId());
	       
	            status = ps.executeUpdate();  
	          
	            con.close();  
	        }catch(Exception ex){ex.printStackTrace();}  
	          
	        return status;  
	}

	@Override
	public int deleteStudent(int studentid) {
		  int status=0;  
	        try{  
	            Connection con = CommonDao.getConnection();  
	            PreparedStatement ps = con.prepareStatement("delete from studentinfo where StudentId = ?;");  
	            ps.setInt(1,studentid); 
	          
	            status = ps.executeUpdate();  
	              
	            con.close();  
	        }catch(Exception e){
	        	e.printStackTrace();
	        }  
	          
	        return status;  
	}

	@Override
	public List<StudentBean> getAllStudents() {
		List<StudentBean> list = new ArrayList<StudentBean>();  
        
        try{  
            Connection con = CommonDao.getConnection();  
            PreparedStatement ps = con.prepareStatement("SELECT * FROM studentinfo;");  
            ResultSet rs = ps.executeQuery();  //row wise work 
            while(rs.next()){  
                StudentBean student = new StudentBean();  
                student.setStudentId(rs.getInt(1));  
                student.setEnrollment(rs.getString(2));  
                student.setName(rs.getString(3));  
                student.setSarname(rs.getString(4));  
                student.setContactNo(rs.getString(5));  
                student.setCity(rs.getString(6));
               
                list.add(student);  
             
            }  
            con.close();  
        	}catch(Exception e){e.printStackTrace();}  
          
        	return list;
	}

	@Override
	public StudentBean getStudentByStudentId(int studentid) {
		 StudentBean student = new StudentBean();  
         
	        try{  
	            Connection con = CommonDao.getConnection();  
	            PreparedStatement ps = con.prepareStatement(" SELECT * from  studentinfo where StudentId=?;");  
	            ps.setInt(1,studentid);  
	            
	            ResultSet rs = ps.executeQuery();  
	            if(rs.next()){  
	            	student.setStudentId(rs.getInt(1));  
	                student.setEnrollment(rs.getString(2)); 
	                student.setName(rs.getString(3));  
	                student.setSarname(rs.getString(4));  
	                student.setContactNo(rs.getString(5));  
	                student.setCity(rs.getString(6)); 
	             
	            }  
	            con.close();  
	        }catch(Exception ex){ex.printStackTrace();}  
	          
	        return student;  
	}

}
