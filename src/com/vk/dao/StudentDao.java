package com.vk.dao;

import java.util.List;

import com.vk.model.StudentBean;

public interface StudentDao {
	
	 	public int saveStudent(StudentBean student); 
	    public int updateStudent(StudentBean student) ;
	    public int deleteStudent(int studentid);
	    public List<StudentBean> getAllStudents();
	    public StudentBean getStudentByStudentId(int studentid);
		
	

}
